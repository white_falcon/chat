let send_btn=document.getElementById('push');
let messages= document.getElementById('messages');

send_btn.onclick=()=>{
    send_message()
};

setInterval(()=>{
    load_message(sessionStorage.getItem('receiver'))
},900)


function load_message(id){
    let recep = document.getElementById('recep');
    recep.innerHTML="Discussion avec "+ document.getElementById('receiver').innerHTML;
    sessionStorage.setItem("receiver",id);
    var xhr =new XMLHttpRequest
    xhr.open("GET","index.php?control=home&task=fetch_message&id="+id,true)
    xhr.send();
    xhr.onreadystatechange=()=>{
        if(xhr.readyState==4){
            messages.innerHTML=xhr.response;
        }
    }
}
function send_message(){
    let message=document.getElementById('msg').value
    async_request("index.php?control=home&task=send_message&message="+message+"&receiver="+sessionStorage.getItem("receiver"));
    message=document.getElementById('msg').value="";
    load_message(sessionStorage.getItem('receiver'));
}

function async_request(url_param){
    var xhr = new XMLHttpRequest;
    xhr.open("GET",url_param,true);
    xhr.send();
}
